# PROJEKT - PREZENTACE

- Pokud děláte projekt samostatně do archivu/repozitáře přidejte seznam
  použitých příkazů (nebo skript s čitelným zdrojovým kódem co tyto příkazy
  automaticky provede) a krátký popis projektu (adresářová struktura,
  potřebný software, atd.).

- Pokud děláte projekt ve skupině do archivu/repozitáře přidejte prezentaci
  projektu (na zhruba 10 až 20 minut), která bude obsahovat alespoň
  následující:

  - informace o navržené XML struktuře pro datové XML dokumenty a jejich
       propojení do jednoho XML dokumentu (můžete popsat i postup převodu
       dat do XML struktury)

  - informace o validaci pomocí DTD a RelaxNG schema

  - informace o generování (X)HTML výstupů

  - informace o generování PDF výstupů

  - informace o generování ePub výstupů (pokud jste se rozhodli
       zpracovat tuto část)

  - informace o pokročilém formátování (X)HTML a PDF výstupů (pokud jste
       se rozhodli zpracovat tuto část)

- Při prezentování ve skupině by měl každý člen skupiny odprezentovat
  alespoň jednu z částí (1-6) popsanou výše. Pokud má skupina menší nebo
  větší počet členů než je počet částí prezentace projektu rozdělte si
  prezentování částí rovnoměrně (tj. složitější části mohou prezentovat
  např. 2 lidé nebo více jednodušších částí zvládne 1 člen skupiny).
