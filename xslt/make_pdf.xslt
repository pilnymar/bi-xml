<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm" margin="3cm"
                                       margin-bottom="1.5cm"
                                       margin-top="1.5cm">
                    <fo:region-body margin-top="1cm" margin-bottom="2cm"/>
                    <fo:region-before extent="1cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:declarations>
                <x:xmpmeta xmlns:x="adobe:ns:meta/">
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/">
                            <dc:title>World Factbook</dc:title>
                        </rdf:Description>
                    </rdf:RDF>
                </x:xmpmeta>
            </fo:declarations>
            <fo:page-sequence master-reference="A4-portrait">

                <fo:static-content flow-name="xsl-region-before">
                    <fo:block font-size="8pt" font-weight="400" text-align="left">
                        <fo:basic-link internal-destination="info" color="blue" text-decoration="none">
                            Content
                        </fo:basic-link>
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-size="10pt" font-weight="400" text-align="center">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="18pt" font-weight="400" text-align="center" id="info">
                        World Factbook
                    </fo:block>
                    <xsl:apply-templates/>
                </fo:flow>

            </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="project/countries">
        <fo:block font-size="15pt">
            Content
        </fo:block>
        <fo:list-block provisional-distance-between-starts="0.2cm" provisional-label-separation="0.1cm">
            <xsl:for-each select="country">
                <xsl:variable name="country_name">
                    <xsl:value-of select="name"/>
                </xsl:variable>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <fo:basic-link internal-destination="{$country_name}" color="blue" text-decoration="none">
                                <xsl:value-of select="$country_name"/>
                            </fo:basic-link>


                            <fo:list-block provisional-distance-between-starts="0.3cm"
                                           provisional-label-separation="0.15cm"
                                           margin-left="20pt">

                                <fo:list-item>
                                    <fo:list-item-label end-indent="label-end()">
                                        <fo:block>
                                            <fo:inline>-</fo:inline>
                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body start-indent="body-start()">
                                        <fo:block font-size="10pt">
                                            <!-- TODO nefunguje link na obrázky-->
                                            <fo:basic-link internal-destination="{$country_name}_pictures" color="blue"
                                                           text-decoration="none">
                                                Pictures
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>

                                <xsl:for-each select="section">
                                    <xsl:variable name="section_name">
                                        <xsl:value-of select="name"/>
                                    </xsl:variable>
                                    <fo:list-item>
                                        <fo:list-item-label end-indent="label-end()">
                                            <fo:block>
                                                <fo:inline>-</fo:inline>
                                            </fo:block>
                                        </fo:list-item-label>
                                        <fo:list-item-body start-indent="body-start()">
                                            <fo:block font-size="10pt">
                                                <fo:basic-link internal-destination="{generate-id(.)}" color="blue"
                                                               text-decoration="none" >
                                                    <xsl:value-of select="concat(upper-case(substring($section_name,1,1)),
														  substring($section_name, 2)
														 )
												  "/>
                                                </fo:basic-link>
                                            </fo:block>
                                        </fo:list-item-body>
                                    </fo:list-item>
                                </xsl:for-each>
                            </fo:list-block>

                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
        <xsl:apply-templates select="country"/>
    </xsl:template>

    <xsl:template match="project/countries/country">
        <xsl:variable name="country_name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <fo:block page-break-before="always"/>
        <fo:block font-size="15pt" font-weight="400" text-align="center" id="{$country_name}" padding-top="15pt">
            <xsl:value-of select="$country_name"/>
        </fo:block>
        <fo:block text-align="center" padding-top="10pt">
            <fo:external-graphic height="40px"
                                 content-width="scale-to-fit"
                                 content-height="scale-to-fit"
                                 scaling="uniform">
                <xsl:attribute name="src">
                    url('./output/html/images/<xsl:value-of select="$country_name"/>/flag.gif')
                </xsl:attribute>
            </fo:external-graphic>
        </fo:block>

        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{$country_name}_pictures">
            Pictures
        </fo:block>

        <fo:block text-align="center" padding-top="5pt" padding-bottom="5pt">
            <fo:inline  vertical-align="text-top">
                <fo:external-graphic width="200px"
                                     content-width="scale-to-fit"
                                     content-height="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('./output/html/images/<xsl:value-of select="$country_name"/>/large.jpg')
                    </xsl:attribute>
                </fo:external-graphic>
                <fo:external-graphic width="200px"
                                     content-width="scale-to-fit"
                                     content-height="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('./output/html/images/<xsl:value-of select="$country_name"/>/map.gif')
                    </xsl:attribute>
                </fo:external-graphic>
            </fo:inline>
        </fo:block>

        <xsl:apply-templates select="section"/>
    </xsl:template>


    <xsl:template match="section">
        <xsl:variable name="section_name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <fo:block font-size="15pt" font-weight="400" text-align="left" margin-top="30pt" id="{generate-id(.)}">
            <xsl:value-of select="concat(upper-case(substring($section_name,1,1)),
														  substring($section_name, 2)
														 )
												  "/>
        </fo:block>

        <xsl:apply-templates select="par"/>
    </xsl:template>

    <xsl:template match="section/par">
        <xsl:variable name="par_name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <fo:block margin-top="5pt">
            <fo:inline font-weight="bold">
                <xsl:value-of select="concat(upper-case(substring($par_name,1,1)),
														  substring($par_name, 2),
														  ':'
														 )
												  "/>
            </fo:inline>

            <xsl:apply-templates select="data"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="subdata">
                <fo:table>
                    <fo:table-column column-width="proportional-column-width(1)"/>
                    <fo:table-body>
                        <xsl:for-each select="subdata">
                            <fo:table-row height="18pt">
                                <fo:table-cell display-align="center">
                                    <fo:block text-align="left" margin="2px">
                                        <xsl:value-of select="text()"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </xsl:for-each>
                    </fo:table-body>
                </fo:table>
            </xsl:when>
            <xsl:otherwise>
                <fo:block margin-top="5pt">
                    <fo:inline font-weight="400">
                        <xsl:value-of select="text()"/>
                    </fo:inline>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

</xsl:stylesheet>
