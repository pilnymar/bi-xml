<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xls="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <html>
            <head>
                <xsl:apply-templates select="/country" mode="title"/>
                <link rel="stylesheet" type="text/css" href="styles.css"/>
            </head>
            <body>
                <header>
                    <nav>
                        <xsl:apply-templates select="/country" mode="menu"/>
                    </nav>
                </header>
                <article>
                    <xsl:apply-templates select="/country" mode="content"/>
                </article>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="country" mode="title">
        <xsl:variable name="name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <title>
            <xsl:value-of select="$name"/> | Webpage
        </title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </xsl:template>

    <xsl:template match="country" mode="menu">
        <xsl:variable name="name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <ul>
            <li><a href="./index.html">Back</a></li>
            <xsl:for-each select="section">
                <xsl:variable name="section_name">
                    <xsl:value-of select="name"/>
                </xsl:variable>
                <li>
                    <a href='#menu{position()}'>
                        <xsl:value-of select="$section_name"/>
                    </a>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="country" mode="content">
        <xsl:variable name="name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <h1>
            <xsl:value-of select="$name"/>
        </h1>
        <section class="images">
            <table>
                <tr>
                    <td><img src="images/{$name}/flag.gif" alt="{$name}" class="flag"/></td>
                    <td><img src="images/{$name}/map.gif" alt="{$name}" class="map"/></td>
                    <td><img src="images/{$name}/large.jpg" alt="{$name}" class="large"/></td>
                </tr>
            </table>
        </section>
        <xls:for-each select="section">
            <xsl:variable name="section_name">
                <xsl:value-of select="name"/>
            </xsl:variable>
            <section id='menu{position()}'>
                <h2>
                    <xsl:value-of select="$section_name"/>
                </h2>
                <xsl:for-each select="par">
                    <xsl:variable name="par_name">
                        <xsl:value-of select="name"/>
                    </xsl:variable>
                    <h3>
                        <xsl:value-of select="$par_name"/>
                    </h3>
                    <xsl:apply-templates select="data"/>
                </xsl:for-each>
            </section>
        </xls:for-each>
    </xsl:template>

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="subdata">
                <table class="sub-data">
                <xsl:for-each select="subdata">
                    <tr>
                        <td>
                            <xsl:value-of select="text()"/>
                        </td>
                    </tr>
                </xsl:for-each>
                </table>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:value-of select="text()"/>
                </p>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
</xsl:stylesheet>
