#!/bin/bash

echo 'Copy data files'
mkdir -p src
cp -r ../data/ ./src

echo 'Making index'
java -jar ../saxon/saxon9he.jar ../generated_source.xml make_index.xslt -o:"index.html"

countries=()
for f in ../data/*; do
  file="${f%.xml}"
  name="$(echo "$file" | rev | cut -d'/' -f 1 | rev)"
  countries+=( "$name" )
done

for country in "${countries[@]}"
do
echo 'Making' "${country}"' page'
java -jar ../saxon/saxon9he.jar src/data/"${country}".xml make_country.xsl -o:"${country}.html"
cp "${country}".html output/html/"${country}".html
rm "${country}".html
done

echo 'Making PDF'
java -jar ../saxon/saxon9he.jar ../generated_source.xml make_pdf.xslt > pdf.fo
fop -fo pdf.fo -pdf countries.pdf 2>/dev/null

rm pdf.fo
rm -r src/data/
unset countries
echo Done.
