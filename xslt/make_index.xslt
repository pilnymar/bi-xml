<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version = '2.0'
                xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

    <xsl:template match="/" >
        <xsl:result-document href="./output/html/index.html">
            <html lang="en">
                <head>
                    <title>Index</title>
                    <link rel="stylesheet" type="text/css" href="styles.css"/>
                </head>
                <body>
                    <article>
                        <h1>Index</h1>
                        <section class="big-menu">
                            <ul>
                                <xsl:apply-templates select="/project/countries/country" mode="index"/>
                            </ul>
                        </section>
                    </article>

                </body>
            </html>
        </xsl:result-document>

        <xsl:apply-templates select="/project/countries/country" mode="page" />
    </xsl:template>

    <xsl:template match="/project/countries/country" mode="index" >
        <xsl:variable name="name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <li>
            <a href="./{$name}.html">
                <img src="images/{$name}/flag.gif" alt="Flag of {$name}" title="{$name} page"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="/project/countries/country" mode="page" >
        <xsl:variable name="name">
            <xsl:value-of select="name"/>
        </xsl:variable>
        <xsl:result-document href="./output/html/{$name}.html">
            <html lang="en">
                <head>
                    <title>
                        <xsl:value-of select="$name" />
                    </title>
                </head>
                <body>
                    <a href="./index.html">Back</a>
                    <h1><xsl:value-of select="$name" /> page</h1>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>
