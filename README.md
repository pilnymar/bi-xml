# BI-XML

## Generování výsledného XML souboru
``xmllint --noent source.xml > generated_source.xml``
## Validace
### DTD
``xmllint --noout --dtdvalid validations/validation.dtd generated_source.xml``
### RelaxNG
``xmllint --noout --relaxng validations/all.rng generated_source.xml``

Pouze jedna země (např. Dánsko):

``xmllint --noout --relaxng validations/one.rng data/Denmark.xml``

## Generování HTML stránek a PDF
Bash skript ve složce xslt generuje HTML stránky do složky xslt/output a také xslt/countries.pdf
```shell script
cd xslt
. transformXSL.sh
``` 